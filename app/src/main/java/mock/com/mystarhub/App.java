package mock.com.mystarhub;

import android.app.Application;
import android.content.SharedPreferences;

/**
 * Created by codesanitar on 17/02/16.
 */
public class App extends Application {

    private static App instance;

    public static App getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public SharedPreferences getPrefs() {
        if (mPrefs == null)
            mPrefs = this.getApplicationContext().getSharedPreferences("pres", 0);
        return mPrefs;
    }

    private SharedPreferences mPrefs;
}
