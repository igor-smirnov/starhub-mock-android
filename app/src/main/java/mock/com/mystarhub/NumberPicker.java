package mock.com.mystarhub;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;

/**
 * Created by codesanitar on 18/02/16.
 */
public class NumberPicker {
    public static AlertDialog getNumberPickerDialog(Context context, String title, final ResultPicker result) {
        final android.widget.NumberPicker numberPicker = new android.widget.NumberPicker(context);
        numberPicker.setLayoutParams(new android.widget.NumberPicker.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        numberPicker.setMaxValue(99999);
        numberPicker.setMinValue(0);
        numberPicker.setValue(1);
        numberPicker.setWrapSelectorWheel(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!title.equals(""))
            builder.setTitle(title);
        builder.setView(numberPicker);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        numberPicker.clearFocus();
                        if (result!=null)
                            result.onGetValue(numberPicker.getValue());
                    }
                });
        return builder.create();
    }

    public interface ResultPicker
    {
        void onGetValue(int value);
    }
}

