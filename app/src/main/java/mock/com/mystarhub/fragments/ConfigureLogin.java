package mock.com.mystarhub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mock.com.mystarhub.App;
import mock.com.mystarhub.R;

/**
 * Created by codesanitar on 17/02/16.
 */
public class ConfigureLogin extends Fragment {

    @Bind(R.id.validity)
    EditText edValidity;
    @Bind(R.id.radio)
    RadioGroup radioGroup;
    @Bind(R.id.identifier)
    EditText edIdentifier;

    @OnClick(R.id.save_button)
    public void saveParams(View view) {
        String auth = "";
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.checkSuccess:
                auth = "SUCCESS";
                break;
            case R.id.checkFailed:
                auth = "FAILED";
                break;
            case R.id.checkCancelled:
                auth = "CANCELLED";
                break;
        }

        App.getInstance().getPrefs().edit().putString("identifier", edIdentifier.getText().toString()).apply();
        App.getInstance().getPrefs().edit().putString("authenticate", auth).apply();
        App.getInstance().getPrefs().edit().putInt("validity", Integer.valueOf(edValidity.getText().toString())).apply();
    }

    public ConfigureLogin() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_configurelogin, container, false);
        ButterKnife.bind(this, view);

        String validity = String.valueOf(App.getInstance().getPrefs().getInt("validity", 10000));
        edValidity.setText(validity);
        edIdentifier.setText( App.getInstance().getPrefs().getString("identifier", "123456789"));
        switch(App.getInstance().getPrefs().getString("authenticate", "SUCCESS"))
        {
            case "SUCCESS": radioGroup.check(R.id.checkSuccess); break;
            case "FAILED": radioGroup.check(R.id.checkFailed); break;
            case "CANCELLED": radioGroup.check(R.id.checkCancelled); break;
        }

        return view;
    }



}
