package mock.com.mystarhub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import mock.com.mystarhub.NumberPicker;
import mock.com.mystarhub.PackageUtils;
import mock.com.mystarhub.R;

/**
 * Created by codesanitar on 17/02/16.
 */
public class Requests extends Fragment {

    @OnClick(R.id.button_open)
    public void openApp(View view) {
        PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", null);
    }

    @OnClick(R.id.button_open_uri)
    public void openAppUri(View view) {
        PackageUtils.startAppUri(getActivity(), "pro.rlclub.b2c.starhub", "shbonus://");
    }

    @OnClick(R.id.button_map)
    public void openMap(View view) {
        Bundle b = new Bundle();
        b.putString("map", null);
        PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
    }

    @OnClick(R.id.button_map_uri)
    public void openMapUri(View view) {
        PackageUtils.startAppUri(getActivity(), "pro.rlclub.b2c.starhub", "shbonus://map");
    }

    @OnClick(R.id.button_feed)
    public void openFeed(View view) {
        Bundle b = new Bundle();
        b.putString("feed", null);
        PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
    }

    @OnClick(R.id.button_feed_uri)
    public void openFeedUri(View view) {
        PackageUtils.startAppUri(getActivity(), "pro.rlclub.b2c.starhub", "shbonus://feed");
    }

    @OnClick(R.id.button_action)
    public void openAction(View view) {
        NumberPicker.getNumberPickerDialog(getActivity(), "set id", new NumberPicker.ResultPicker() {
            @Override
            public void onGetValue(int value) {
                Bundle b = new Bundle();
                b.putString("feed", null);
                b.putString("id", String.valueOf(value));
                PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
            }
        }).show();
    }

    @OnClick(R.id.button_action_uri)
    public void openActionUri(View view) {
        NumberPicker.getNumberPickerDialog(getActivity(), "set id", new NumberPicker.ResultPicker() {
            @Override
            public void onGetValue(int value) {
                PackageUtils.startAppUri(getActivity()
                        , "pro.rlclub.b2c.starhub", "shbonus://feed?id=" + value);
            }
        }).show();
    }

    @OnClick(R.id.button_category_list)
    public void openCategoryList(View view) {
        Bundle b = new Bundle();
        b.putString("category", null);
        PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
    }

    @OnClick(R.id.button_category_list_uri)
    public void openCategoryListUri(View view) {
        PackageUtils.startAppUri(getActivity(), "pro.rlclub.b2c.starhub", "shbonus://category");
    }

    @OnClick(R.id.button_category_actions)
    public void openActionCategory(View view) {
        NumberPicker.getNumberPickerDialog(getActivity(), "set id", new NumberPicker.ResultPicker() {
            @Override
            public void onGetValue(int value) {
                Bundle b = new Bundle();
                b.putString("category", null);
                b.putString("id", String.valueOf(value));
                PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
            }
        }).show();
    }

    @OnClick(R.id.button_category_actions_uri)
    public void openActionCategoryUri(View view) {
        NumberPicker.getNumberPickerDialog(getActivity(), "set id", new NumberPicker.ResultPicker() {
            @Override
            public void onGetValue(int value) {
                PackageUtils.startAppUri(getActivity()
                        , "pro.rlclub.b2c.starhub", "shbonus://category?id=" + value);
            }
        }).show();
    }



    @OnClick(R.id.button_vendors)
    public void openVendorsList(View view) {
        Bundle b = new Bundle();
        b.putString("vendor", null);
        PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
    }

    @OnClick(R.id.button_vendors_uri)
    public void openVendorsListUri(View view) {
        PackageUtils.startAppUri(getActivity(), "pro.rlclub.b2c.starhub", "shbonus://vendor");
    }

    @OnClick(R.id.button_vendor_actions)
    public void openActionVendor(View view) {
        NumberPicker.getNumberPickerDialog(getActivity(), "set id", new NumberPicker.ResultPicker() {
            @Override
            public void onGetValue(int value) {
                Bundle b = new Bundle();
                b.putString("vendor", null);
                b.putString("id", String.valueOf(value));
                PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
            }
        }).show();
    }

    @OnClick(R.id.button_vendor_actions_uri)
    public void openActionVenorUri(View view) {
        NumberPicker.getNumberPickerDialog(getActivity(), "set id", new NumberPicker.ResultPicker() {
            @Override
            public void onGetValue(int value) {
                PackageUtils.startAppUri(getActivity()
                        , "pro.rlclub.b2c.starhub", "shbonus://vendor?id=" + value);
            }
        }).show();
    }

    @OnClick(R.id.button_balance)
    public void openBalance(View view) {
        Bundle b = new Bundle();
        b.putString("balance", null);
        PackageUtils.startApp(getActivity(), "pro.rlclub.b2c.starhub", "pro.rlclub.b2c.starhub.MYSH", b);
    }

    @OnClick(R.id.button_balance_uri)
    public void openBalanceUri(View view) {
        PackageUtils.startAppUri(getActivity(), "pro.rlclub.b2c.starhub", "shbonus://balance");
    }


    public Requests() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_requests, container, false);
        ButterKnife.bind(this, view);
        return view;

    }



}