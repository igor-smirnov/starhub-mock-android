package mock.com.mystarhub;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.os.ResultReceiver;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import mock.com.mystarhub.adapters.ViewPagerAdapter;
import mock.com.mystarhub.fragments.ConfigureLogin;
import mock.com.mystarhub.fragments.Requests;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    @Bind(R.id.viewpager)ViewPager viewPager;
    @Bind(R.id.tabs)TabLayout tabLayout;
    final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                processCall();
            }
        });

        processCall();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter.addFragment(new ConfigureLogin(), "Login");
        adapter.addFragment(new Requests(), "Requests");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("login");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.login , 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("requests");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.request, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        processCall();
    }

    public void processCall()
    {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("action"))
            {
               String action = extras.getString("action");
                if (action!=null && action.equals("AUTHORIZE"))
                {
                    String pkg = extras.getString("package"); // do we need it?
                    ResultReceiver callback = extras.getParcelable("callback");
                    if (callback != null) {
                        callback.send(RESULT_OK, getParams());
                    }
                }
            }

        }
    }

    public Bundle getParams()
    {
        Bundle result = new Bundle();
        result.putString("identifier", App.getInstance().getPrefs().getString("identifier", "123456789"));
        result.putString("authenticate", App.getInstance().getPrefs().getString("authenticate", "SUCCESS"));
        result.putInt("validity", App.getInstance().getPrefs().getInt("validity", 10000));
        return result;
    }

}
