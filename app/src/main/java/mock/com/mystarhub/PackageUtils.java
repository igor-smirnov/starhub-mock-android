package mock.com.mystarhub;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;

import java.util.List;

/**
 * Created by codesanitar on 16/02/16.
 */
public class PackageUtils {
    public static void startApp(Context context, final String appPackageName, final String call, Bundle extras) {
        Intent intent = new Intent(call);
        initIntent(context, appPackageName, extras, intent);
    }

    public static void startAppUri(Context context, final String appPackageName, final String uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        initIntent(context, appPackageName, null, intent);
    }

    private static void initIntent(Context context, String appPackageName, Bundle extras, Intent intent) {
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        boolean isIntentSafe = activities.size() > 0;

        if (isIntentSafe) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (extras!=null)
                intent.putExtras(extras);
            context.startActivity(intent);
        }
        else {
            appMarket(context, appPackageName);
        }
    }

    private static void appMarket(Context context, String appPackageName) {
        Intent intent;
        try {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + appPackageName));
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
            context.startActivity(intent);

        }
    }
}
